# -*- coding: utf-8 -*-

import os
import pytest
import tuxput
from conftest import bucket


@pytest.fixture
def client(mock_s3_client):
    """ Default configuration flask client """
    with tuxput.create_app().test_client() as client:
        yield client


@pytest.fixture
def mock_env_no_overwrite(monkeypatch):
    monkeypatch.setenv("ALLOW_UPLOAD_OVERWRITE", "false")


@pytest.fixture
def client_no_root(mock_env_no_overwrite):
    with tuxput.create_app().test_client() as client:
        yield client


def test_unauth_upload(client):
    pass


def test_enforce_no_overwrite(client):
    pass


def test_root(client):
    response = client.get("/")
    assert response.status_code == 200


# @pytest.mark.parametrize(
#    "url",
#    [
#        ("favicon.ico"),  # explicit route
#        ("foo/"),  # does not exist
#        ("foo"),  # does not exist
#        ("build1/foo"),  # does not exist
#        ("build1/foo.json"),  # does not exist
#        ("build1/foo/"),  # does not exist
#        ("build1/empty_file"),  # empty object
#    ],
# )
# def test_404s(url, client, client_no_root):
#    """ test that a 404 is served """
#    for c in [client, client_no_root]:
#        response = c.get("http://localhost/" + url)
#        if response.status_code == 302:
#            response = c.get(response.location)
#        assert response.status_code == 404


# @pytest.mark.parametrize(
#    "key",
#    [
#        ("ipsum.txt"),
#        ("build1/dtbs/zynq-zturn.dtb"),
#        ("build1/build.log"),
#        ("build2/dtbs.json"),
#    ],
# )
# def test_file_urls(key, client, client_no_root):
#    """ Test that files are served """
#    for c in [client, client_no_root]:
#        response = c.get("http://localhost/" + key)
#        assert response.status_code == 302
#        assert response.location.startswith(f"https://{bucket}.s3.amazonaws.com/{key}")


# @pytest.mark.parametrize(
#    "key,contains",
#    [
#        ("build1/dtbs/", ["zynq-zybo.dtb", "zynq-zed.dtb"]),
#        ("build1/", ["build.log", "build_definition.json", "bmeta.json"]),
#        ("build2/", ["dtbs.json", "build.log"]),
#    ],
# )
# def test_directory_urls_html(key, contains, client, client_no_root):
#    """ Test that files are served """
#    for c in [client, client_no_root]:
#        response = c.get("http://localhost/" + key)
#        assert response.status_code == 200
#        assert "Parent Directory".encode("utf-8") in response.data
#        for filename in contains:
#            assert filename.encode("utf-8") in response.data


# @pytest.mark.parametrize(
#    "key,length",
#    [
#        ("build1/dtbs/", 4),
#        ("build1/dtbs", 4),
#        ("build1/", 4),
#        ("build1", 4),
#        ("build2/", 4),
#    ],
# )
# def test_directory_urls_json_length(key, length, client, client_no_root):
#    """ Test that all files and folders are listed in json """
#    for c in [client, client_no_root]:
#        response = c.get(f"http://localhost/{key}?export=json")
#        if response.status_code == 302:
#            response = c.get(response.location)
#        assert response.status_code == 200
#        assert response.content_type == "application/json"
#        files_and_folders = [
#            item["Url"] for item in response.json["files"] + response.json["folders"]
#        ]
#        assert len(files_and_folders) == length


# @pytest.mark.parametrize(
#    "parent,children",
#    [
#        ("build1/dtbs", []),
#        ("build1", ["dtbs"]),
#        ("build2", []),
#    ],
# )
# def test_directory_urls_json(parent, children, client, client_no_root):
#    """ Test that in each parent directory, child directories are listed correctly in json """
#    for c in [client, client_no_root]:
#        response = c.get(f"http://localhost/{parent}?export=json")
#        if response.status_code == 302:
#            response = c.get(response.location)
#        assert response.status_code == 200
#        assert response.content_type == "application/json"
#        folders = [item["Url"] for item in response.json["folders"]]
#        for child in children:
#            assert f"http://localhost/{parent}/{child}/" in folders
#        if not children:
#            assert len(folders) == 0
#

# @pytest.mark.parametrize(
#    "key",
#    [
#        ("build1/dtbs/zynq-zturn.dtb"),
#        ("build1/build.log"),
#        ("build2/dtbs.json"),
#    ],
# )
# def test_file_urls_json(key, client, client_no_root):
#    """ For each file, request its directories json export and verify that it is listed """
#    for c in [client, client_no_root]:
#        dirname = os.path.dirname(key)
#        response = c.get(f"http://localhost/{dirname}?export=json")
#        if response.status_code == 302:
#            response = c.get(response.location)
#        files = [item["Url"] for item in response.json["files"]]
#        assert f"http://localhost/{key}" in files


# def test_export_not_supported(client, client_no_root):
#    """ Test an invalid export option """
#    for c in [client, client_no_root]:
#        response = c.get("http://localhost/build1/?export=yaml")
#        assert response.status_code == 501
#        assert "format not supported".encode("utf-8") in response.data


# def test_export_redirect_with_arguments(client, client_no_root):
#    for c in [client, client_no_root]:
#        # Note no "/" after directory, before export
#        response = c.get("http://localhost/build1?export=json")
#        assert response.status_code == 302
#        assert response.location == "http://localhost/build1/?export=json&"
