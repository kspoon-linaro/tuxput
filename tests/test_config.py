# -*- coding: utf-8 -*-

import pytest
from tuxput.config import TuxConfig

bucket = "foo-bucket"
region = "us-west-1"
site_title = "Tuxput"


@pytest.fixture
def mock_env_empty(monkeypatch):
    monkeypatch.delenv("S3_BUCKET", raising=False)
    monkeypatch.delenv("S3_REGION", raising=False)
    monkeypatch.delenv("SITE_TITLE", raising=False)
    monkeypatch.delenv("AUTH_FILE", raising=False)
    monkeypatch.delenv("ALLOW_UPLOAD_OVERWRITE", raising=False)


@pytest.fixture
def mock_env_minimum(monkeypatch):
    monkeypatch.setenv("S3_BUCKET", bucket)
    monkeypatch.setenv("S3_REGION", region)


def test_no_s3_bucket(mock_env_empty):
    with pytest.raises(AssertionError) as excinfo:
        config = TuxConfig()  # noqa: F841
    assert "Required env var S3_BUCKET not set" in str(excinfo.value)


def test_no_s3_region(mock_env_empty, monkeypatch):
    monkeypatch.setenv("S3_BUCKET", bucket)
    with pytest.raises(AssertionError) as excinfo:
        config = TuxConfig()  # noqa: F841
    assert "Required env var S3_REGION not set" in str(excinfo.value)


def test_minimum(mock_env_minimum, monkeypatch):
    config = TuxConfig()
    assert config.S3_BUCKET == bucket
    assert config.S3_REGION == region
    assert config.ALLOW_UPLOAD_OVERWRITE == False  # defaults to True
    assert isinstance(config.ALLOW_UPLOAD_OVERWRITE, bool)  # ensure it's bool
    assert config.AUTH_FILE
    assert isinstance(config.AUTH_FILE, str)
