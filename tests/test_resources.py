# -*- coding: utf-8 -*-

import pytest
from tuxput import resources
from conftest import bucket, region


@pytest.fixture
def s3_handle(mock_s3_client):
    """ Return an resources.s3_server handle """
    yield resources.s3_server(bucket, region)


@pytest.mark.parametrize(
    "key",
    [
        ("tuxput.passwd"),
        ("build1/foo.sh"),
    ],
)
def test_create_signed_url(key, s3_handle):
    url = s3_handle.create_signed_url(key)
    assert url.startswith(f"https://{bucket}.s3.amazonaws.com/{key}")


@pytest.mark.parametrize(
    "key,exists",
    [
        ("tuxput.passwd", True),
        ("build1/foo.sh", True),
        ("build2/foo.sh", False),
    ],
)
def test_key_exists(key, exists, s3_handle):
    assert s3_handle.key_exists(key) == exists
